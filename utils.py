# This Python file uses the following encoding: utf-8
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as col
import os.path

def plot_policy(M, policy):
    # generate a random number as name suffix
    name_suffix = 1
    maze_map = np.copy(M.maze)
    maze_map[maze_map==1] = 4

    for i, u in enumerate(policy):
        x, y = M.state_mapping.inverse[i]
        maze_map[x][y] = u

        # define individual colors as hex values
    cpool = ['#3498DB', '#28B463', '#EC7063', '#FDEBD0', '#424949']

    # set the colormap
    color_map = col.ListedColormap(cpool, 'indexed')

    # plot the policy
    plt.imshow(maze_map, cmap=color_map, interpolation='nearest')

    # set the legend of the colormap
    cbar = plt.colorbar()
    cbar.ax.get_yaxis().set_ticks([])
    for j, legend in enumerate(['Down','Right','Up','Left','Wall']):
        cbar.ax.text(2, (2*j + 1) / 10.0, legend, horizontalalignment='left',
                     verticalalignment='center')
    # save the plot
    fname = 'policy'+str(name_suffix)+'.eps'
    while os.path.isfile(fname):
        name_suffix += 1
        fname = 'policy'+str(name_suffix)+'.eps'

    plt.savefig(fname)
