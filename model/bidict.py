# This Python file uses the following encoding: utf-8

class BiDict(dict):
    '''
    This class implements a bijective mapping
    '''
    def __init__(self, *args, **kwargs):
        super(BiDict, self).__init__(*args, **kwargs)
        self.inverse = {}
        for key, value in self.iteritems():
            self.inverse[value] = key

    def __setitem__(self, key, value):
        super(BiDict, self).__setitem__(key, value)
        self.inverse[value] = key

    def __delitem__(self, key):
        del self.inverse[self[key]]
        super(BiDict, self).__delitem__(key)
