# This Python file uses the following encoding: utf-8
import numpy as np
from bidict import BiDict

class MazeMDP(object):
    def __init__(self, maze, actions,
                 start, goal, g_mode='no_move_cost', alpha=0.9):

        '''
        maze:     a two-dimensional numpy array with 1 indicating walls and 0
                  free spaces
        actions:  a dictionary of possible actions, for instance:
                  {'up':(-1,0), 'down':(1,0), 'left':(0,-1), 'right':(0,1)}
        start:    the coordinates of the start point
        goal:     the coordinates of the goal point
        g_mode:   determines if moving in the maze has a cost
        alpha:    discount factor of the MDP, between 0 and 1

        '''

        # read in the problem and declares the member variables
        self.maze = maze
        self.actions = actions
        self.start = start
        self.goal = goal
        self.g_mode = g_mode
        self.alpha = alpha
        self.state_mapping = BiDict({})
        self.action_mapping = BiDict({})
        self.n_actions = None
        self.n_states = None
        self.P = None
        self.g = None
        self.trajectory = None

        # calculate the transition probability and the cost function
        self._calc_state_mapping()
        self._calc_action_mapping()
        self.calc_P()
        self.calc_g()

    def state_xy2num(self, state_xy):
        return self.state_mapping[state_xy]

    def state_num2xy(self, state_num):
        return self.state_mapping.inverse[state_num]

    def act(self, state_xy, action_string):

        if self.is_goal(state_xy):
            return state_xy

        action_xy = np.array(self.actions[action_string])
        state_xy = np.array(state_xy)
        next_state_xy = state_xy + action_xy
        next_state_xy = tuple(next_state_xy)
        state_xy = tuple(state_xy)

        if self.is_legal(next_state_xy):
            return next_state_xy
        else:
            return state_xy

    def calc_g(self):
        '''
        g[(u,i,j)] is the cost from state i to state j after taking action u.
        Note that the cost of illegal transitions does not play a role as it
        will be multiplied by zero transition probabilities.
        '''
        goal_num = self.state_xy2num(self.goal)

        if self.g_mode=='no_move_cost':
            self.g = np.zeros((self.n_actions, self.n_states, self.n_states))
            self.g[:,:,goal_num] = -100 # entering the goal state has a reward
        elif self.g_mode=='with_move_cost':
            self.g = np.ones((self.n_actions, self.n_states, self.n_states))
            self.g[:,:,goal_num] = 0 # entering the goal state has no cost

        self.g[:,goal_num,:] = 0 # staying at the goal state costs nothing

    def calc_P(self):
        '''
        self.P[(u,i,j)] returns the probability of reaching state j from
        state i after taking action u
        '''
        self.P = np.zeros((self.n_actions,
                           self.n_states,
                           self.n_states))

        for (u,i,j), _ in np.ndenumerate(self.P):
            if j==self._act(i,u):
                self.P[(u,i,j)] = 1

    def is_legal(self, state_xy):

        if self.maze[state_xy] == 0:
            return True
        else:
            return False

    def is_goal(self, state_xy):

        if state_xy == self.goal:
            return True
        else:
            return False

    def _calc_state_mapping(self):
        '''
        Construct a bijective state mapping between the representation
        in maze coordinates and integers
        '''
        s = 0
        for (x,y), value in np.ndenumerate(self.maze):
            if value == 0:
                self.state_mapping[(x,y)] = s
                s += 1
        self.n_states = len(self.state_mapping)

    def _calc_action_mapping(self):
        '''
        Construct a bijective action mapping between the representation
        in strings and integers
        '''
        a = 0
        for action_string in self.actions.iterkeys():
            self.action_mapping[action_string] = a
            a += 1
        self.n_actions = len(self.action_mapping)

    def _act(self, state_num, action_num):
        state_xy = self.state_mapping.inverse[state_num]
        action_string = self.action_mapping.inverse[action_num]
        next_state_xy = self.act(state_xy, action_string)
        next_state_num = self.state_mapping[next_state_xy]
        return next_state_num
