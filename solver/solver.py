# This Python file uses the following encoding: utf-8
import numpy as np

class Solver(object):
    def __init__(self, model):
        self.model = model

        # initialize
        self.n_states = model.n_states
        self.n_actions = model.n_actions
        self.alpha = model.alpha
        self.n_iterations_OVI = 20

    def g_mu(self, policy):
        g_mu = np.zeros(self.n_states)
        M = self.model
        for i in range(self.n_states):
            u = policy[i]
            g_mu[i] = np.inner(M.P[u,i,:], M.g[u,i,:])
        return g_mu

    def P_mu(self, policy):
        P_mu = np.zeros((self.n_states, self.n_states))
        for (i,j), _ in np.ndenumerate(P_mu):
            u = policy[i]
            P_mu[(i,j)] = self.model.P[(u,i,j)]
        return P_mu

    def random_policy(self):
        return np.random.randint(self.n_actions, size=self.n_states)

    def run(self, policy, start=None):
        if start==None:
            start = self.model.state_xy2num(self.model.start)

    def solve(self, mode='VI',
              n_iterations=None,
              n_iterations_OVI=None):
        '''
        a wrapper for different algorithms
        '''
        policy = None
        J = None

        if mode == 'PI_VI':
            policy = self.policy_iteration(n_iterations, 'VI')
            J = self.policy_evaluation(policy,'VI')

        if mode == 'PI_OVI':
            if n_iterations_OVI is not None:
                self.n_iterations_OVI = n_iterations_OVI
            policy = self.policy_iteration(n_iterations, 'OVI')
            J = self.policy_evaluation(policy,'OVI')

        elif mode == 'PI_LE':
            policy = self.policy_iteration(n_iterations, 'LE')
            J = self.policy_evaluation(policy,'LE')

        elif mode == 'VI':
            J = self.value_iteration(None, n_iterations)
            policy = self.policy_improvement(J)

        return policy, J

    def T(self, J, policy=None):
        '''
        policy=None  : T is the optimal Bellman operator
        policy!=None : T is the Bellman operator w.r.t the given policy
        '''
        TJ = np.zeros(self.n_states)
        if policy is not None:
            g_mu = self.g_mu(policy)
            P_mu = self.P_mu(policy)
            TJ = g_mu + self.alpha*np.dot(P_mu, J)
        else:
            g = self.model.g
            P = self.model.P

            for i in range(self.n_states):
                J_i = np.zeros(self.n_actions)
                for u in range(self.n_actions):
                    g_ui = np.dot(P[u,i,:], g[u,i,:])
                    J_ui = np.dot(P[u,i,:], J)
                    J_i[u] = g_ui + self.alpha*J_ui
                TJ[i] = np.min(J_i)

        return TJ

    def policy_improvement(self, J):
        g = self.model.g
        P = self.model.P
        new_policy = self.random_policy()

        for i in range(self.n_states):
            J_i = np.zeros(self.n_actions)
            for u in range(self.n_actions):
                g_ui = np.dot(P[u,i,:], g[u,i,:])
                J_ui = np.dot(P[u,i,:], J)
                J_i[u] = g_ui + self.alpha*J_ui
            new_policy[i] = int(np.argmin(J_i))
        return new_policy

    def policy_iteration(self, n_iterations=None, mode='LE'):
        policy = self.random_policy()
        if n_iterations is not None:
            for i in range(n_iterations):
                J_mu = self.policy_evaluation(policy, mode)
                policy = self.policy_improvement(J_mu)

        else:
            converged = False
            cnt = 0
            while not converged:
                J_mu = self.policy_evaluation(policy, mode)
                new_policy = self.policy_improvement(J_mu)
                if np.equal(new_policy,policy).all():
                    converged = True
                    # print cnt
                policy = new_policy
                cnt+=1

        return policy

    def value_iteration(self, policy=None, n_iterations=None):
        '''
        If policy=None, the optimal Bellman's operator will be applied;
        otherwise, the Bellman's operator w.r.t the policy will be applied.
        If n_iterations=None, the VI will run until convergence; otherwise, it
        will only run a fixed number times.
        '''
        J = np.zeros(self.n_states)
        if n_iterations is not None:
            # fixed number of iterations
            for k in range(n_iterations):
                J = self.T(J, policy)
        else:
            converged = False
            cnt = 0
            while not converged:
                TJ = self.T(J, policy)
                diff = np.linalg.norm(TJ - J, np.inf)
                if diff<np.finfo(np.float).eps:
                    converged = True
                J = TJ
                cnt+=1
        return J

    def policy_evaluation(self, policy, mode='LE'):
        J_mu = np.zeros(self.n_states)
        if mode=='OVI':
            J_mu = self.value_iteration(policy, self.n_iterations_OVI)
        elif mode=='VI':
            J_mu = self.value_iteration(policy)
        elif mode=='LE':
            g_mu = self.g_mu(policy)
            P_mu = self.P_mu(policy)
            I = np.eye(self.n_states)
            J_mu = np.dot(np.linalg.inv(I-self.alpha*P_mu), g_mu)
        return J_mu
